# Java Exceptions, Logging, Unit Testing
Simple project used to illustrate Exception handling, Log4J and JUnit.

### Exceptions
Our custom exceptions in this project extend the base `Exception` class:
- `DatabaseException`
- `ValidationException`

### Logging
Log4j

```
<dependency>
    <groupId>log4j</groupId>
    <artifactId>log4j</artifactId>
    <version>1.2.12</version>
</dependency>
```

File: resources/log4j.properties

```
# Define the root logger with appender file
log = /usr/home/log4j
log4j.rootLogger = DEBUG, FILE

# Define the file appender
log4j.appender.FILE=org.apache.log4j.FileAppender
log4j.appender.FILE.File=${log}/log.out

# Define the layout for file appender
log4j.appender.FILE.layout=org.apache.log4j.PatternLayout
log4j.appender.FILE.layout.conversionPattern=%m%n
```

Location of log files: /usr/home/log4j

### Unit Testing
Select `WorkerTest` from Configurations dropdown (located top right)

Structure of a unit test:
- Arrange
- Act
- Assert

Example:
```
    @Test
    @DisplayName("Worker: Test Add")
    @Tag("Math")
    void testWorkerAdd() {
        // Arrange
        var w = new Worker();

        // Act
        var result = w.add(1, 2);

        // Assert
        assertEquals(result, 3, "Add() should return 3.");
    }
```
