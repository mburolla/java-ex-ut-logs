import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

public class WorkerTest {

    //
    // BEFORE
    //

    @BeforeAll
    static void initAll() { }

    @BeforeEach
    void init() { }

    //
    // TESTS
    //

    @Test
    @DisplayName("Worker: Test Add")
    @Tag("Math")
    void testWorkerAdd() {
        // Arrange
        var w = new Worker();

        // Act
        var result = w.add(1, 2);

        // Assert
        assertEquals(result, 3, "Add() should return 3.");
    }

    //
    // AFTER
    //

    @AfterEach
    void tearDown() { }

    @AfterAll
    static void tearDownAll() { }
}
