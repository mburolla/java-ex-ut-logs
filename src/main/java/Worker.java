import Exceptions.*;
import org.apache.log4j.*;

public class Worker {

    //
    // Constructors
    //

    public Worker() {}

    //
    // Public Methods
    //

    public void exceptions() {
        System.out.println("*** Exceptions ***");

        try {
            doWork();
            doMoreWork("Joe");
        }
        catch (DatabaseException | ValidationException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
//        catch (DatabaseException de) {  // Cannot do this: Exception has already been caught.
//            de.printStackTrace();
//        }
    }

    public void logging() {
        System.out.println("*** Logging ***");

        Logger logger = Logger.getLogger(Worker.class); //log = /usr/home/log4j

        ConsoleAppender ca = new ConsoleAppender();
        ca.setThreshold(Level.INFO);
        ca.setLayout(new PatternLayout("%d %p [%c] - %m%n"));
        ca.activateOptions();
        Logger.getRootLogger().addAppender(ca);

        try {
            doSomething();
        }
        catch (Exception e) {
            logger.debug("I am a debug message.", e);
            logger.info("I am an info message.");
        }
    }

    private void doSomething() throws Exception {
        // do some work here
        throw new Exception("I bombed, error processing something...");
    }

    public int add(int operand1, int operand2) {
        return operand1 + operand2;
    }

    //
    // Private Methods
    //

    private void doWork() throws DatabaseException {
        int a = 0;
        int b = 1;
        //throw new DatabaseException("Failed to satisfy foreign key constraint.");
    }

    private void doMoreWork(String name) throws ValidationException {
        if (name.matches(".*\\d.*")) { // Searches for a number.
            throw new ValidationException("Error: Name contains a number.");
        }
    }
}
